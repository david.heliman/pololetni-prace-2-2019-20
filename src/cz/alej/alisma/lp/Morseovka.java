import java.util.Scanner;
public class Morseovka {
  public static void main(String[] args){
    Scanner vstup = new Scanner(System.in);
    char[] pismena = { ' ', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
    String[] morseovka = { "  /  ", ". ___", "___ . . .", "___ . ___ .", "___ . .", ".", ". . ___ .", "___ ___ .", ". . . .", ". .", ". ___ ___ ___", "___ . ___", ". ___ . .",  "___ ___", "___ .", "___ ___ ___", ". ___ ___ .", "___ ___ . ___", ". ___ .", ". . .", "_", ". . ___", ". . . ___", ". ___ ___", "___ . . ___", "___ . ___ ___", "___ ___ . ."};
    String textKeZmene = "";
    String novyText = "";
    System.out.println("zadej text ke změně");
    textKeZmene = vstup.nextLine();
    textKeZmene = textKeZmene.toLowerCase();
    for (int i = 0; i < textKeZmene.length(); i++) {
      for (short j = 0; j < 37; j++) {
        if (textKeZmene.charAt(i) == pismena[j]) {
          novyText += morseovka[j];
          novyText += "   ";
          break;
        }        
      }
    }
    System.out.println("text v morseovce");
    System.out.println(novyText);
  }
}
//spolupráce se Sebastian Černý a Jan Sklenička
